// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://opensource.org/licenses/mit-license.php )
// +----------------------------------------------------------------------
// | Author: NickBai
// +----------------------------------------------------------------------

module.exports = {
	
	// 登录token
	'LOGIN_TOKEN': 'login_token',
	
	// 用户id
	'UID': 'uid',
	
	// 用户状态
	'USER_INFO': 'user_info',
	
	// 过期时间
	'EXPIRES_TIME': 'expires_time',
	
	// 登录返回地址
	'LOGIN_BACK_URL': 'login_back_url'
}